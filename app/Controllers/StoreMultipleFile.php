<?php namespace App\Controllers;

use App\Models\ImageModel;

class StoreMultipleFile extends BaseController
{

    public function index()
    {

        $data = [];
        helper(['form', 'url']);

        $db = \Config\Database::connect();
        $builder = $db->table('image');

        $validated = $this->validate([
            'photo' => [
                'uploaded[photo]',
                'mime_in[photo,image/jpg,image/jpeg,image/gif,image/png]',
                'max_size[photo,4096]',
            ],
        ]);

        $msg = 'Please select a valid file';

        if ($validated) {
            $file = $this->request->getFile('photo');
            $img_dir = uniqid();
            $file->move('assets/uploads/images/views', $img_dir . '.png');

            $data = [
                'img_dir' => base_url() . '/assets/uploads/images/views/' . $img_dir . '.png',
                'img_name' => uniqid(),
                'img_type' => '.png',
                'id' => session()->get('id'),
                'img_size' => $file->getSize(),
            ];

            $save = $builder->insert($data);
            $msg = 'Files has been uploaded';

            $query = $db->query('SELECT img_name, img_dir, img_type, img_size FROM image');
            $results = $query->getResult();
            $data['results'] = $results;
        }

        echo (view('templates/header', $data));
        echo (view('storeMultipleFile'));
        echo (view('templates/footer'));

        //    return redirect()->to( base_url('/dashboard') )->with('msg', $msg);

    }

public function pagination(){
    $data = [];
    helper(['form']);
    
    $model = new ImageModel();

    $data = [
        'users' => $model->paginate(10),
        'pager' => $model->pager,
    ];

    $query = $db->query('SELECT img_name, img_dir, img_type, img_size FROM image');
    $results = $query->getResult();
    $data['results'] = $results;
    
    echo (view('templates/header', $data));
    echo (view('storeMultipleFile'));
    echo (view('templates/footer'));
}

public function loadRecord($rowno=0){

    $data = [];
    helper(['form']);
    
    $model = new ImageModel();
    
    $rowperpage = 5;

    
    if($rowno != 0){
      $rowno = ($rowno-1) * $rowperpage;
    }

    $query = $db->query('SELECT img_name, img_dir, img_type, img_size FROM image');
    $results = $query->getResult();
    $data['results'] = $results;
    $data['rowno'] = $rowno;

    echo json_encode($data);


}

}