<?php namespace App\Models;

use CodeIgniter\Model;

class ImageModel extends Model
{
    protected $table = 'image';
    protected $allowedFields = ['img_name', 'img_dir', 'img_type', 'img_size'];
}
