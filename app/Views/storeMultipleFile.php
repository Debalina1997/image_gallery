<!DOCTYPE html>
<html>
<head>
  <title>Codeigniter 4 Image upload with preview example</title>

 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="/assets/css/style.css">

  <style type="text/css">
    a {
      padding-left: 5px;
      padding-right: 5px;
      margin-left: 5px;
      margin-right: 5px;
    }
    </style>

</head>
<body>
 <div class="container">
    <br>

    <?php if (session('msg')): ?>
        <div class="alert alert-info alert-dismissible">
            <?=session('msg')?>
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        </div>
    <?php endif?>

    <div class="row">
      <div class="col-md-9">
        <form action="/storeMultipleFile" name="ajax_form" id="ajax_form" method="post" accept-charset="utf-8" enctype="multipart/form-data">
         <div class="row">
          <div class="form-group col-md-6">
            <label for="formGroupExampleInput">Name</label>
            <input type="file" name="photo" class="form-control" id="photo" onchange="readURL(this);" accept=".png, .jpg, .jpeg" />
          </div>

          <div class="row">
              <div class="form-group col-md-12">
                <button type="submit" id="send_form" class="btn btn-success">Submit</button></br></br>
            </div>
          </div>


  <?php
if (isset($results)) {
    foreach ($results as $row): ?>
     <div class="form-group col-md-6">
          <img class="d-block w-100" src="<?php echo ($row->img_dir); ?>" /></br></br>
      </div>
     <div class="row">
          <div class="col-md-6">
            <h6>Name :<?php echo $row->img_name; ?> </h6></br></br>
          </div>
      </div>
      <div class="row">
         <div class="col-md-6">
            <h6>Type :<?php echo $row->img_type ?></h6></br></br>
         </div>
      </div>
      <div class="row">
          <div class="col-md-6">
            <h6>Size :<?php echo $row->img_size ?></h6>
          </div>
      </div>
        <?php endforeach;}?>

        </form>
       </div>
      </div>

    </div>
  </div>

  <div style='margin-top: 10px;' id='pagination'></div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   <script type='text/javascript'>
   $(document).ready(function(){

     $('#pagination').on('click','a',function(e){
       e.preventDefault();
       var pageno = $(this).attr('data-ci-pagination-page');
       loadPagination(pageno);
     });

     loadPagination(0);

     function loadPagination(pagno){
       $.ajax({
         url: '<?=base_url()?> /assets/uploads/images/view'+pagno,
         type: 'get',
         dataType: 'json',
         success: function(response){
            $('#pagination').html(response.pagination);
            createTable(response.result,response.row);
         }
       });
     }

     function createTable(image,sno){
       sno = Number(sno);
       $('#postsList tbody').empty();
       for(index in result){
         var img_dir = image[index].img_dir;
         var img_name = result[index].img_name;
         var img_type = result[index].img_type;
         var img_size = result[index].img_size;
         sno+=1;

          var tr = "<tr>";
          tr += "<td>"+ sno +"</td>";
          tr += "<td><a href='"+ link +"' target='_blank' >"+ title +"</a></td>";
          tr += "<td>"+ content +"</td>";
          tr += "</tr>";
          $('#postsList tbody').append(tr);
 
        }
      }
    });

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>