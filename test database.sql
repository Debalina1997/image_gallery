-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'sports','2014-06-01 00:35:07','2014-05-31 00:34:33'),(2,'Electronics','2014-06-01 00:35:07','2014-05-31 00:34:33'),(3,'Homegoods','2014-06-01 00:35:07','2014-05-31 00:34:54');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contacts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'John doe','9748849146','jdoe@yopmail.com','barrackpore'),(2,'name','12345678','debalinachatterjee11@gmail.com','      wej'),(3,'samsung','12345678','debalinachatterjee11@gmail.com','      r6r'),(4,'','','','      '),(5,'','','','      '),(6,'samsung','12345678','deba11@gmail.com','      abcd'),(7,'samsung','83838','mail@mail.com','      df');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `image` (
  `img_id` int NOT NULL AUTO_INCREMENT,
  `img_name` varchar(255) DEFAULT NULL,
  `img_dir` varchar(255) DEFAULT NULL,
  `img_type` varchar(50) DEFAULT NULL,
  `img_size` varchar(50) DEFAULT NULL,
  `id` int DEFAULT NULL,
  PRIMARY KEY (`img_id`),
  KEY `id` (`id`),
  CONSTRAINT `image_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user_s` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` VALUES (1,'60408e84c7317','http://localhost:8080/assets/uploads/images/views/60408e84c6f3b.png','.png','6402',3),(2,'60408e9092970','http://localhost:8080/assets/uploads/images/views/60408e90926b1.png','.png','6138',3),(3,'60408e98ed690','http://localhost:8080/assets/uploads/images/views/60408e98ed145.png','.png','7195',3),(4,'60408eb2f2d34','http://localhost:8080/assets/uploads/images/views/60408eb2f2abb.png','.png','6017',3),(5,'60408ebb406db','http://localhost:8080/assets/uploads/images/views/60408ebb404ab.png','.png','6237',3),(6,'60408ec8a118f','http://localhost:8080/assets/uploads/images/views/60408ec8a0f18.png','.png','15570',3),(7,'60408ed582aab','http://localhost:8080/assets/uploads/images/views/60408ed582862.png','.png','8722',3),(8,'60408ee37f52c','http://localhost:8080/assets/uploads/images/views/60408ee37f2da.png','.png','4402',3),(9,'60408f2683e64','http://localhost:8080/assets/uploads/images/views/60408f2683bba.png','.png','7581',3),(10,'60408f628cecf','http://localhost:8080/assets/uploads/images/views/60408f628cc2e.png','.png','3968',3),(11,'60408f6a772b2','http://localhost:8080/assets/uploads/images/views/60408f6a76f4b.png','.png','13208',3),(12,'6040935c5d249','http://localhost:8080/assets/uploads/images/views/6040935c5d00c.png','.png','13208',3),(13,'604099c3d4a07','http://localhost:8080/assets/uploads/images/views/604099c3d47a1.png','.png','13208',3),(14,'604099fda7e99','http://localhost:8080/assets/uploads/images/views/604099fda7c42.png','.png','13208',3),(15,'60409a2c390e3','http://localhost:8080/assets/uploads/images/views/60409a2c38e92.png','.png','13208',3),(16,'6041cffb3b771','http://localhost:8080/assets/uploads/images/views/6041cffb3b535.png','.png','8722',3),(17,'6041d00e6253a','http://localhost:8080/assets/uploads/images/views/6041d00e622e6.png','.png','12099',3),(18,'6041d0bb9eebe','http://localhost:8080/assets/uploads/images/views/6041d0bb9ec6e.png','.png','12099',3),(19,'6041d585d751a','http://localhost:8080/assets/uploads/images/views/6041d585d6fdb.png','.png','12099',3),(20,'604222e266303','http://localhost:8080/assets/uploads/images/views/604222e0cf73b.png','.png','12099',NULL),(21,'6042348a5c5ee','http://localhost:8080/assets/uploads/images/views/6042348a5c0e7.png','.png','12099',NULL),(22,'6045c40dc5855','http://localhost:8080/assets/uploads/images/views/6045c40dc5269.png','.png','7581',3),(23,'6045c426ed730','http://localhost:8080/assets/uploads/images/views/6045c426ed25e.png','.png','7581',3),(24,'6045c8b6a7d54','http://localhost:8080/assets/uploads/images/views/6045c8b6a7b0a.png','.png','7581',3),(25,'604606535fd28','http://localhost:8080/assets/uploads/images/views/604606535fa7b.png','.png','7581',NULL),(26,'60462658e53bb','http://localhost:8080/assets/uploads/images/views/60462658e517c.png','.png','7581',NULL),(27,'604708b0eb181','http://localhost:8080/assets/uploads/images/views/604708b0eaa7d.png','.png','15405',3),(28,'60470f9e2826d','http://localhost:8080/assets/uploads/images/views/60470f9e2802a.png','.png','15405',3);
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `img`
--

DROP TABLE IF EXISTS `img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `img` (
  `img_id` int NOT NULL AUTO_INCREMENT,
  `img_name` varchar(255) DEFAULT NULL,
  `img_dir` varchar(255) DEFAULT NULL,
  `img_type` varchar(50) DEFAULT NULL,
  `img_size` varchar(50) DEFAULT NULL,
  `id` int DEFAULT NULL,
  PRIMARY KEY (`img_id`),
  KEY `id` (`id`),
  CONSTRAINT `img_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `img`
--

LOCK TABLES `img` WRITE;
/*!40000 ALTER TABLE `img` DISABLE KEYS */;
INSERT INTO `img` VALUES (1,'bird2.jpg','upload/deba11@gmail.com/bird2.jpg','image/jpeg','5819',4),(2,'bird3.jpg','upload/deba11@gmail.com/bird3.jpg','image/jpeg','7195',4),(3,'bird5.jpg','upload/deba11@gmail.com/bird5.jpg','image/jpeg','3835',4),(4,'bird8.jpg','upload/deba11@gmail.com/bird8.jpg','image/jpeg','7746',4),(5,'bird4.jpg','upload/deba11@gmail.com/bird4.jpg','image/jpeg','16466',4),(6,'bird7.jpg','upload/deba11@gmail.com/bird7.jpg','image/jpeg','9106',4),(7,'bird5.jpg','upload/deba11@gmail.com/bird5.jpg','image/jpeg','3835',4),(8,'download.jpg','upload/mc@gmail.com/download.jpg','image/jpeg','6017',8),(9,'flower.jpg','upload/mc@gmail.com/flower.jpg','image/jpeg','7721',8),(10,'flower1.jpg','upload/mc@gmail.com/flower1.jpg','image/jpeg','6237',8),(11,'flower2.jpg','upload/mc@gmail.com/flower2.jpg','image/jpeg','3968',8),(12,'flower3.jpg','upload/mc@gmail.com/flower3.jpg','image/jpeg','7581',8),(13,'images.jpg','upload/mc@gmail.com/images.jpg','image/jpeg','4402',8),(14,'nature1.jpg','upload/ac@gmail.com/nature1.jpg','image/jpeg','12099',15),(15,'nature2.jpg','upload/ac@gmail.com/nature2.jpg','image/jpeg','15405',15),(16,'nature3.jpg','upload/ac@gmail.com/nature3.jpg','image/jpeg','13208',15),(17,'nature4.jpg','upload/ac@gmail.com/nature4.jpg','image/jpeg','8722',15);
/*!40000 ALTER TABLE `img` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `posts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `body` text NOT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'New post','New-post','In the last section, we went over some basic concepts of the framework by writing a class that references static pages.','2021-02-15 16:59:22'),(2,'ci4','ci4','codeigniter 4 is greate','2021-02-15 18:18:45'),(3,'ci4','ci4','codeigniter 4 is greate','2021-02-15 18:21:45'),(4,'ci4','ci4','codeigniter 4 is greate','2021-02-15 18:33:14'),(5,'ci4','ci4','Codeigniter is great','2021-02-15 18:33:48'),(6,'ci4','ci4','Codeigniter is great','2021-02-15 18:35:20'),(7,'framework','framework','farme work is good','2021-02-15 18:37:27'),(8,'other frame work','other-frame-work','other frame work is also good','2021-02-15 18:39:45');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_s`
--

DROP TABLE IF EXISTS `product_s`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_s` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `product_name` varchar(128) DEFAULT NULL,
  `description` text,
  `price` double DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id` int NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `id` (`id`),
  CONSTRAINT `product_s_ibfk_1` FOREIGN KEY (`id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_s`
--

LOCK TABLES `product_s` WRITE;
/*!40000 ALTER TABLE `product_s` DISABLE KEYS */;
INSERT INTO `product_s` VALUES (11,'Basketball','A ball used in the NBA.',290,'2020-08-02 12:04:03','2020-08-06 13:59:18',1),(12,'Gatorade','This is a very good drink for athletes.',37,'2020-08-02 12:14:29','2020-08-06 13:59:18',1),(13,'Lenovo Laptop','My business partner.',62300,'2020-06-01 01:13:45','2020-05-31 09:13:39',2),(14,'Trash Can','It will help you maintain cleanliness.',297,'2020-08-02 12:16:08','2020-08-06 13:59:18',3),(15,'Mouse','Very useful if you love your computer.',1089,'2020-08-02 12:17:58','2020-08-06 13:59:18',2),(16,'Earphone','You need this one if you love music.',499,'2020-08-02 12:18:21','2020-08-06 13:59:18',2),(17,'Pillow','Sleeping well is important.',147,'2020-08-02 12:18:56','2020-08-06 13:59:18',3),(21,'samsung','new phone',9000,NULL,'2021-01-22 11:46:56',1);
/*!40000 ALTER TABLE `product_s` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `product_name` varchar(128) DEFAULT NULL,
  `description` text,
  `price` double DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (2,'Gatorade','This is a very good drink for athletes.',37,'2020-08-02 12:14:29','2020-08-06 13:59:18'),(3,'Eye Glasses12','It will make you read better.',749,'2020-08-02 12:15:04','2021-01-19 09:07:52'),(4,'Trash Can','It will help you maintain cleanliness.',297,'2020-08-02 12:16:08','2020-08-06 13:59:18'),(5,'Mouse','Very useful if you love your computer.',1089,'2020-08-02 12:17:58','2020-08-06 13:59:18'),(6,'Earphone15','You need this one if you love music.',499,'2020-08-02 12:18:21','2021-01-19 11:38:28'),(7,'Pillow','Sleeping well is important.',147,'2020-08-02 12:18:56','2020-08-06 13:59:18'),(198,'Laptop','Work from home',32000,NULL,'2021-01-19 11:39:35');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phoneno` int NOT NULL,
  `address` varchar(255) NOT NULL,
  `admin` int DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `upadated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (4,'Debalina','Chatterjee','deba11@gmail.com',1234567890,'Begharia, Kolkata',1,'$2y$10$qTk5h22jACXR1bS9ZS589OkgMCpyPil4McyEmknkK2Bll3O23Pr0S','2021-02-08 14:10:23',NULL,'photo/deba11@gmail.com/bird.jpg',NULL),(8,'Mandira','Chowdhuri','mc@gmail.com',1234567888,'Begharia, Kolkata',0,'$2y$10$fuS9IpjR9t5djtk8tDGZoeD3Evnt.FoayWJDt5f8r/QSeRxi05aAm','2021-02-09 11:15:58',NULL,'photo/mc@gmail.com/bird9.jpg',NULL),(15,'Arka','Majumdar','ac@gmail.com',1234567890,'Belur, Howrah',0,'$2y$10$KJJQvcLa117yyv7bTlmEMuTJ/Hp1.vRpqnAKX0JSCdNjXhh6nYyZW','2021-02-09 19:26:14',NULL,'photo/ac@gmail.com/nature.jpg',NULL),(16,'Progya','Saha','pg@gmail.com',1234567888,'sodepur,kolkata',0,'$2y$10$crwrLgioVangaFDv7wA3OevzgSSjV5r6BUCd7gPnlQYfEv2UcPppu','2021-02-23 02:32:01',NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_data`
--

DROP TABLE IF EXISTS `user_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_data` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `message` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_data`
--

LOCK TABLES `user_data` WRITE;
/*!40000 ALTER TABLE `user_data` DISABLE KEYS */;
INSERT INTO `user_data` VALUES (1,'Debalina','chatterjee','mail@mail.com','well'),(3,'Progya','Saha','email@mail.com','nice'),(5,'Atma','Dey','atma1@gmail.com','nice'),(6,'arka','majumdar','gmail@mail.com','good'),(7,'banhni','ray','br@gmail.com','well'),(10,'abhjit','ghosh','ag@gmail.com','not good'),(11,'soumi','shee','ss@mail.com','avg.'),(12,'babi','chakraborty','babic@gmail.com','good');
/*!40000 ALTER TABLE `user_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_s`
--

DROP TABLE IF EXISTS `user_s`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_s` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phoneno` int NOT NULL,
  `address` varchar(255) NOT NULL,
  `admin` int DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_s`
--

LOCK TABLES `user_s` WRITE;
/*!40000 ALTER TABLE `user_s` DISABLE KEYS */;
INSERT INTO `user_s` VALUES (1,'Debalina','Chatterjee','dc@gmail.com',1234567890,'Begharia, Kolkata',NULL,'$2y$10$VQz2LtiVXsTaNyLsKe.hBOXk3W3aTX5zwORIUCmdhWjFqUzB78PSq','2021-02-23 22:49:28',NULL,'http://localhost/assets/uploads/images/users/1.png','2021-02-25 23:01:00'),(2,'Progya','Saha','ps@gmail.com',2147483647,'sodepur,kolkata',NULL,'$2y$10$whbPy7hlUZ9YcWzIf.hn1uLTPA0StYGG6vosEPjr.Fw0YAdYn94F.','2021-02-23 22:55:58',NULL,'http://localhostpublic/uploads/images/users/2.png','2021-02-25 05:28:40'),(3,'Arka','Majumdar','am@gmail.com',1234567888,'Belur, Howrah',NULL,'$2y$10$4LU0L6xcDvmbx48Brw0cJOoW.5aexxLHwbULow5h.IEe/E7.4cBli','2021-02-23 22:57:09',NULL,'http://localhost/assets/uploads/images/users/3.png','2021-02-25 22:56:11'),(12,'Mandira','Chowdhuri','ma@gmail.com',2147483647,'Begharia, Kolkata',NULL,'$2y$10$n5MhSMprmaLpyGapLjSx6OjyuI.yi.lRqpsUZ4peIdqQt65qIVxMG','2021-02-25 06:49:12',NULL,'http://localhostpublic/uploads/images/users/12.png','2021-02-25 06:49:36'),(13,'Atmadip','Dey','ad@gmail.com',2147483647,'kolkata',NULL,'$2y$10$YwuYPQNHdTGxtXvmzv80bOR8/ymJDYxWtLQ2YwW7slWjZK2.EJcNC','2021-02-26 03:50:33',NULL,NULL,NULL),(14,'Anwesha','Roy','ar@gmail.com',1234567886,'Contai, Midnapur',NULL,'$2y$10$apmFEbgHoAiqYbsso.YTpuRcfJGQxBHqnQHh656I.rQmiUnanskme','2021-02-28 23:39:42',NULL,NULL,NULL),(15,'Raktim','Pratihar','rp@gmail.com',2147483647,'Gorbeta, Midnapur',NULL,'$2y$10$bGe5U//YU6T6LovJtEGZzuSunRw9HC3xJ99VkkRELH.O5Bc8JAZtq','2021-03-01 02:18:50',NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_s` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Deba','$2y$10$FMULbiPxZgBDPOe0q9M/XOdBOiEVXrB38/nr3pwYqSChakbfddoMy','2021-01-25 05:56:18'),(2,'debaa','$2y$10$kX/p.JDmh7abfqT6WMGwCe6TGIANKXmvrL454B59BwjRNXfOp6zxa','2021-01-26 20:49:48'),(3,'Babi','$2y$10$I2RWpqxISXe8TgPFU7BZvuQSG8PY.rYwMC0Hqg3o7KvU2A534S5I.','2021-01-26 20:55:53'),(4,'Debu','$2y$10$ckLQERyxsKgKPJlolbZrHOE1ft4.it3.KLYhbb..wCwTp/kaGRp9C','2021-01-26 20:56:20'),(5,'Debalina','$2y$10$EaVkfPipCvnmnzSiGWrCrORAUbzJcerA.JBpBbyF5xgvdaQaHFB6q','2021-01-26 21:19:23'),(6,'Babita','$2y$10$fwaQSZYd/xlx7C2gHutHOeksxSYPKURkB2OEoFWNDEzBC6chqfiJC','2021-01-26 21:25:00'),(7,'Sroeta','$2y$10$IcQXCTDl3RkNxySX4ixJZeL4zbJ3j9wlWnXuIak9uDnXDc0esuaIK','2021-01-26 21:27:34'),(8,'Debuu','$2y$10$DMjxr/4.KSMkxJA6.Iy9q.dEdWEwQXV/PA09V8EesNEywo0/WXRL.','2021-01-26 21:34:29'),(9,'Debabrata','$2y$10$M7lPRY4mpgQqqxuVOamSk.CTHceZIruBhtd4mfsIO9gMZ9ejuNnS6','2021-01-26 22:03:42'),(10,'babii','$2y$10$3Y58las.Vm.fXBvBerJXGO5jCNuNYAwaKHYN3iWWXPfBsYDP40HF6','2021-02-02 04:25:42');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'test'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-09 18:49:51
